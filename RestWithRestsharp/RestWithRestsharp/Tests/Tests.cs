﻿using Newtonsoft.Json;
using NUnit.Framework;
using RestSharp;
using RestWithRestsharp.Models;
using System.Collections.Generic;

namespace RestWithRestsharp.Tests
{
    class Tests
    {
        private const string Host = "https://jsonplaceholder.typicode.com";
        private RestClient _restClient;

        [SetUp]
        public void Setup()
        {
            _restClient = new RestClient(Host);
        }

        [Test]
        public void VerifyThatTheNumberOfTodosForUserIdOneIsEven()
        {
            // Arrange
            var request = new RestRequest("/todos", Method.GET);


            // Act
            var restSharpResponse = _restClient.Execute(request);
            var response = JsonConvert.DeserializeObject<List<Todo>>(restSharpResponse.Content);

            // Assert
            Assert.That(response, Has.Count.GreaterThan(0));
        }

        [Test]
        public void VerifyThatTheMaximumUserIdOneIsNine()
        {
            // Arrange
            var request = new RestRequest("/todos", Method.GET);


            // Act
            var restSharpResponse = _restClient.Execute(request);
            var response = JsonConvert.DeserializeObject<List<Todo>>(restSharpResponse.Content);

            // Assert
            Assert.That(response, Has.Count.LessThan(9));
        }

        [Test]
        public void VerifyThatTheMaximumUserIdOneIsEighty()
        {
            // Arrange
            var request = new RestRequest("/todos", Method.GET);


            // Act
            var restSharpResponse = _restClient.Execute(request);
            var response = JsonConvert.DeserializeObject<List<Todo>>(restSharpResponse.Content);

            // Assert
            Assert.That(response, Has.Count.LessThanOrEqualTo(80));
        }

        [Test]
        public void VerifyThatTheMaximumUserIdIsFiveHundred()
        {
            // Arrange
            var request = new RestRequest("/comments", Method.GET);


            // Act
            var restSharpResponse = _restClient.Execute(request);
            var response = JsonConvert.DeserializeObject<List<Comment>>(restSharpResponse.Content);
            
            // Assert
            Assert.That(response, Has.Count.EqualTo(500));
        }

        [Test]
        public void VerifyCountOfUserIdWithIdOne()
        {
            // Arrange
            var request = new RestRequest("/comments", Method.GET);


            // Act
            var restSharpResponse = _restClient.Execute(request);
            var response = JsonConvert.DeserializeObject<List<Photo>>(restSharpResponse.Content);

            // Assert
            Assert.That(response, Has.Count.EqualTo(500));
        }
    }
}