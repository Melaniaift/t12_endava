Resourses Service on https://jsonplaceholder.typicode.com/

 
--GET

For:
  /todos

 

    Verify that the number of todo�s for userId 1 is even

 

    Verify that the maximum id for userId 1 is 9

 

    Verify that the maximum id for userId 1 is 80

 

  /comments

 

    Get maximum id for comment.
    You�re solution should work for a randomly distribute comments by id, not ascending or descending order

 

  /photos

 

    Get the number of photos for album with id 1

 

    Find the album with the most photos

 

  /albums

 

    Get the number of albums for user 4

 

    Get the shortest title for an album
    

--POST

For:

  /posts

    Additional endpoint constraints:
        The field "title" should only accept string values
        The field "userId" should not be greater than 100
        The field "body" should be greater in length than "title" only if "title" is a substring of "body"
        The fileds "title" and "userId" are required.
        Two resources cannot have the same "title"
    
    Create a new valid post and verify the response status to be the appropriated one.

    Create a new post having a different value type for "title" (other than string) and verify the actual status with the appropriate status code. ("title" is supposed to be a string value)

    Create additional tests validating the endpoint's constraints.


--PUT

For:

    /posts/{id}

    Additional endpoint constraints:
        The parameter "id" and the body field "id" must be the same.
        The body should contain all fields. ("title", "body", "userId", "id")

    Update a resource and verify the response status to be the appropriated one.

    Create additional tests validating the endpoint's constraints.



--DELETE

For:

    /posts/{id}

    Note: Delete method is idempotent.

    Delete a resource and verify the response status to be the appropriated one.

    Delete the same resource and verify the response status to be the appropriate one.

    Delete a resource that does not exist and verify the response status to be the appropriated one.

Suggestions:
    Avoid as much as possible hard coding values.
    Keep in mind OOP and SOLID principles when creating and structuring your classes! (especially on S(ingle Responsability Principle))